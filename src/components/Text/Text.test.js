import React from 'react';
import renderer from 'react-test-renderer';

import Text from './Text';

test('should match snapshot', () => {
    const tree = renderer.create(<Text>Hello world</Text>).toJSON();
    expect(tree).toMatchSnapshot();
});
