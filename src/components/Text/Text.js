import React from 'react';
import PropTypes from 'prop-types';
import { Text as RNText } from 'react-native';

const sizes = {
    sm: 16,
    md: 21,
    lg: 28
};

const Text = ({ size, style, ...rest }) => (
    <RNText style={[{ fontSize: sizes[size], textAlign: 'center' }, style]} {...rest} />
);

Text.propTypes = {
    size: PropTypes.oneOf(['sm', 'md', 'lg']),
    style: PropTypes.object
};

Text.defaultProps = {
    size: 'md'
};

export default Text;
