import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';

const Container = ({ children }) => <View style={styles.container}>{children}</View>;

Container.propTypes = {
    children: PropTypes.node.isRequired
};

export default Container;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 50,
        marginBottom: 50,
        marginLeft: 25,
        marginRight: 25
    }
});
