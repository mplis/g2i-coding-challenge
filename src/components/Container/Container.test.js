import React from 'react';
import { Text } from 'react-native';
import renderer from 'react-test-renderer';

import Container from './Container';

test('should match snapshot', () => {
    const tree = renderer
        .create(
            <Container>
                <Text>Hello world</Text>
            </Container>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
