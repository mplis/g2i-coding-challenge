import React from 'react';
import renderer from 'react-test-renderer';

import Question from './Question';

test('should match snapshot', () => {
    const question =
        'In &quot;Metal Gear Solid 2&quot;, you will see through the eyes of Psycho Mantis if you go first person during his boss fight.';
    const tree = renderer.create(<Question question={question} />).toJSON();
    expect(tree).toMatchSnapshot();
});
