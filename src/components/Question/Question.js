import React from 'react';
import PropTypes from 'prop-types';
import Text from '../Text/Text';
import htmlDecode from '../../utils/htmlDecode/htmlDecode';

const Question = ({ question, ...rest }) => (
    <Text style={{ fontWeight: 'bold' }} {...rest}>
        {htmlDecode(question)}
    </Text>
);

Question.propTypes = {
    question: PropTypes.string.isRequired
};

export default Question;
