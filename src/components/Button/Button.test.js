import React from 'react';
import renderer from 'react-test-renderer';

import Button from './Button';

test('should match snapshot', () => {
    const tree = renderer.create(<Button title="Begin" onPress={() => {}} />).toJSON();
    expect(tree).toMatchSnapshot();
});
