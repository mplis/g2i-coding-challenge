import React from 'react';
import axios from 'axios';
import { ActivityIndicator } from 'react-native';
import Container from './components/Container/Container';
import HomeScreen from './screens/HomeScreen/HomeScreen';
import QuestionScreen from './screens/QuestionScreen/QuestionScreen';
import ResultsScreen from './screens/ResultsScreen/ResultsScreen';
import ErrorScreen from './screens/ErrorScreen/ErrorScreen';

export default class App extends React.Component {
    state = {
        index: 0,
        questions: [],
        answers: [],
        screen: 'Home',
        loading: false
    };

    componentDidMount() {
        this.getQuestions();
    }

    navigate = screen => {
        this.setState({ screen });
    };

    getQuestions = () => {
        this.setState({ loading: true });
        return axios
            .get('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
            .then(({ data: { results } }) => this.setState({ questions: results, loading: false }))
            .catch(err => {
                console.error(err);
                this.setState({ loading: false });
                this.navigate('Error');
            });
    };

    answerQuestion = answer => {
        this.setState(({ answers, questions, index }) => {
            const newAnswers = [...answers];
            // storing these as strings so they can more simply be compared to the correct answers and displayed
            newAnswers[index] = answer ? 'True' : 'False';

            const newIndex = index + 1;
            let newState = { answers: newAnswers, index: newIndex };

            // navigate to Results screen if all questions have been answered
            if (newIndex >= questions.length) {
                newState.screen = 'Results';
            }
            return newState;
        });
    };

    restartQuiz = () => {
        this.getQuestions();
        this.setState({ index: 0, answers: [] });
        this.navigate('Home');
    };

    render() {
        const { index, questions, answers, screen, loading } = this.state;
        if (loading) {
            return (
                <Container>
                    <ActivityIndicator size="large" />
                </Container>
            );
        }

        const commonProps = { navigate: this.navigate };
        switch (screen) {
            case 'Questions':
                return (
                    <QuestionScreen
                        {...commonProps}
                        question={questions[index]}
                        answerQuestion={this.answerQuestion}
                        questionIndex={index}
                        numQuestions={questions.length}
                    />
                );
            case 'Results':
                return (
                    <ResultsScreen
                        {...commonProps}
                        questions={questions}
                        answers={answers}
                        restartQuiz={this.restartQuiz}
                    />
                );
            case 'Error':
                return <ErrorScreen />;
            case 'Home':
            default:
                return <HomeScreen {...commonProps} />;
        }
    }
}
