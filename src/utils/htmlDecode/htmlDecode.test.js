import htmlDecode from './htmlDecode';

test('should match snapshot', () => {
    const text =
        'In &quot;Metal Gear Solid 2&quot;, you will see through the eyes of Psycho Mantis if you go first person during his boss fight.';
    const decodedText = htmlDecode(text);
    expect(decodedText).toMatchSnapshot();
});
