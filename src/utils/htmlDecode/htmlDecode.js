import { AllHtmlEntities } from 'html-entities';
const entities = new AllHtmlEntities();

export default html => entities.decode(html);
