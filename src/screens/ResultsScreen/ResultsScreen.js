import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View } from 'react-native';
import Text from '../../components/Text/Text';
import Button from '../../components/Button/Button';
import Container from '../../components/Container/Container';
import Result from './components/Result';

const ResultsScreen = ({ questions, answers, restartQuiz }) => {
    const numQuestions = questions.length;
    const numCorrect = questions.reduce((acc, question, index) => {
        return question.correct_answer === answers[index] ? acc + 1 : acc;
    }, 0);
    return (
        <ScrollView>
            <Container>
                <Text size="lg">
                    You got {numCorrect} / {numQuestions} correct
                </Text>
                {questions.map((question, i) => {
                    const userAnswer = answers[i];
                    return <Result key={i} question={question} userAnswer={userAnswer} />;
                })}
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <Button title="Play Again?" onPress={restartQuiz} />
                    </View>
                </View>
            </Container>
        </ScrollView>
    );
};

ResultsScreen.propTypes = {
    questions: PropTypes.arrayOf(
        PropTypes.shape({
            correct_answer: PropTypes.oneOf(['True', 'False']).isRequired,
            question: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    answers: PropTypes.arrayOf(PropTypes.oneOf(['True', 'False']).isRequired).isRequired,
    restartQuiz: PropTypes.func.isRequired
};

export default ResultsScreen;
