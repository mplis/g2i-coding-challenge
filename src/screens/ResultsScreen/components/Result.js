import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Text from '../../../components/Text/Text';
import Question from '../../../components/Question/Question';

const Result = ({ question, userAnswer }) => {
    const correctAnswer = question.correct_answer;
    const isCorrect = correctAnswer === userAnswer;
    const color = isCorrect ? 'blue' : 'darkorange';
    return (
        <View style={{ margin: 10 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View>
                    <Text style={{ color }} size="lg">
                        {isCorrect ? '+' : '-'}
                    </Text>
                </View>
                <View style={{ margin: 5 }}>
                    <Question question={question.question} style={{ color, textAlign: 'left' }} />
                </View>
            </View>
            <Text size="sm" style={{ fontStyle: 'italic' }}>
                You said {userAnswer}
            </Text>
        </View>
    );
};

Result.propTypes = {
    question: PropTypes.shape({
        correct_answer: PropTypes.oneOf(['True', 'False']).isRequired,
        question: PropTypes.string.isRequired
    }).isRequired,
    userAnswer: PropTypes.oneOf(['True', 'False']).isRequired
};

export default Result;
