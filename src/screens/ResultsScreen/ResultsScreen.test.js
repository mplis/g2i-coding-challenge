import React from 'react';
import renderer from 'react-test-renderer';

import ResultsScreen from './ResultsScreen';
import Result from './components/Result';

describe('ResultsScreen', () => {
    test('should match snapshot', () => {
        const questions = [
            {
                correct_answer: 'True',
                question:
                    'In &quot;Metal Gear Solid 2&quot;, you will see through the eyes of Psycho Mantis if you go first person during his boss fight.'
            }
        ];
        const answers = ['True'];
        const tree = renderer
            .create(<ResultsScreen questions={questions} answers={answers} restartQuiz={() => {}} />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

describe('Result', () => {
    test('should match snapshot', () => {
        const question = {
            correct_answer: 'True',
            question:
                'In &quot;Metal Gear Solid 2&quot;, you will see through the eyes of Psycho Mantis if you go first person during his boss fight.'
        };
        const userAnswer = 'True';
        const tree = renderer.create(<Result question={question} userAnswer={userAnswer} />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
