import React from 'react';
import renderer from 'react-test-renderer';

import QuestionScreen from './QuestionScreen';

test('should match snapshot', () => {
    const question = {
        category: 'Entertainment: Video Games',
        question:
            'In &quot;Metal Gear Solid 2&quot;, you will see through the eyes of Psycho Mantis if you go first person during his boss fight.'
    };
    const tree = renderer
        .create(<QuestionScreen question={question} answerQuestion={() => {}} questionIndex={0} numQuestions={10} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});
