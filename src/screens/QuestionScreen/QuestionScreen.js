import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Text from '../../components/Text/Text';
import Button from '../../components/Button/Button';
import Container from '../../components/Container/Container';
import Question from '../../components/Question/Question';

const QuestionScreen = ({ question, answerQuestion, questionIndex, numQuestions }) => {
    return (
        <Container>
            <View style={{ flex: 1 }}>
                <Text style={{ fontStyle: 'italic' }}>{question.category}</Text>
            </View>
            <View style={{ flex: 10, justifyContent: 'center' }}>
                <Question question={question.question} />
            </View>
            <View style={{ flexDirection: 'row', flex: 5, alignItems: 'center' }}>
                <View style={{ flex: 1, margin: 10 }}>
                    <Button title="True" onPress={() => answerQuestion(true)} />
                </View>
                <View style={{ flex: 1, margin: 10 }}>
                    <Button title="False" onPress={() => answerQuestion(false)} />
                </View>
            </View>
            <View style={{ flex: 1 }}>
                <Text>
                    {questionIndex + 1} of {numQuestions}
                </Text>
            </View>
        </Container>
    );
};

QuestionScreen.propTypes = {
    question: PropTypes.shape({
        question: PropTypes.string.isRequired,
        category: PropTypes.string.isRequired
    }).isRequired,
    answerQuestion: PropTypes.func.isRequired,
    questionIndex: PropTypes.number.isRequired,
    numQuestions: PropTypes.number.isRequired
};

export default QuestionScreen;
