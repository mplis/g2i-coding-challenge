import React from 'react';
import Text from '../../components/Text/Text';
import Container from '../../components/Container/Container';

const ErrorScreen = () => (
    <Container>
        <Text size="lg">An error occurred. Please try again later.</Text>
    </Container>
);

export default ErrorScreen;
