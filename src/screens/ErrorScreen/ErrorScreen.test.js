import React from 'react';
import renderer from 'react-test-renderer';

import ErrorScreen from './ErrorScreen';

test('should match snapshot', () => {
    const tree = renderer.create(<ErrorScreen />).toJSON();
    expect(tree).toMatchSnapshot();
});
