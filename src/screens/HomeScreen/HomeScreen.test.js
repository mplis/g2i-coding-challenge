import React from 'react';
import renderer from 'react-test-renderer';

import HomeScreen from './HomeScreen';

test('should match snapshot', () => {
    const tree = renderer.create(<HomeScreen navigate={() => {}} />).toJSON();
    expect(tree).toMatchSnapshot();
});
