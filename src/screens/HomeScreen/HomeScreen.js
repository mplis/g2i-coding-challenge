import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Text from '../../components/Text/Text';
import Button from '../../components/Button/Button';
import Container from '../../components/Container/Container';

const HomeScreen = ({ navigate }) => (
    <Container>
        <Text size="lg">Welcome to the Trivia Challenge!</Text>
        <View>
            <Text>You will be presented with 10 True or False questions.</Text>
            <Text>Can you score 100%?</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
                <Button title="Begin" onPress={() => navigate('Questions')} />
            </View>
        </View>
    </Container>
);

HomeScreen.propTypes = {
    navigate: PropTypes.func.isRequired
};

export default HomeScreen;
